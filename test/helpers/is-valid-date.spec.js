import { expect } from "chai";

import { isValidDate } from '../../dist';


describe("isValidDate()", function() {

  it("checks valid date", function() {
    expect(isValidDate(new Date("Tue Apr 02 2019 19:19:47 GMT+0800 (Hong Kong Standard Time)"))).to.equal(true);
  });

  it("checks invalid Date without Date constructor", function() {
    expect(isValidDate("Tue Apr 02 2019 19:19:47 GMT+0800 (Hong Kong Standard Time)")).to.equal(false);
  });

  it("checks invalid Date with 'undefined'", function() {
    expect(isValidDate('undefined')).to.equal(false);
  });

  it("checks invalid Date with undefined", function() {
    expect(isValidDate(undefined)).to.equal(false);
  });

  it("checks invalid Date with 'null'", function() {
    expect(isValidDate('null')).to.equal(false);
  });

  it("checks invalid Date with null", function() {
    expect(isValidDate(null)).to.equal(false);
  });
});
