import { expect } from "chai";

import { deductDate } from '../../dist';


describe("deductDate()", function() {

  it("checks basic", function() {
    let sampleDate = new Date('2019-10-17T07:07:08.667Z');
    let date = deductDate(sampleDate, 10);
    expect(date.getDate()).to.equal(7);
  });
});
