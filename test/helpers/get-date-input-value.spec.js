import { expect } from "chai";

import { getDateInputValue } from '../../dist';


describe("getDateInputValue()", function() {

  it("checks basic", function() {
    expect(getDateInputValue("Tue Apr 02 2019 19:19:47 GMT+0800 (Hong Kong Standard Time)")).to.equal('2019-04-02');
  });
});
