import { expect } from "chai";

import { truncate } from '../../dist';


describe("truncate()", function() {

  it("checks basic", function() {
    let text = truncate('hello world', 2);
    expect(text).to.equal('he...');
  });
});
