import { expect } from "chai";

import { isInDateRange } from '../../dist';


describe("isInDateRange()", function() {

  it("checks lower date range", function() {
    let testingDate = new Date('2019-10-13T07:07:08.667Z');
    let midRangeDate = new Date('2019-10-17T07:07:08.667Z');

    expect(isInDateRange(testingDate, 5, midRangeDate, 5)).to.equal(true);
  });

  it("checks higher date range", function() {
    let testingDate = new Date('2019-10-21T07:07:08.667Z');
    let midRangeDate = new Date('2019-10-17T07:07:08.667Z');

    expect(isInDateRange(testingDate, 5, midRangeDate, 5)).to.equal(true);
  });

  it("checks when behind lower date range", function() {
    let testingDate = new Date('2019-10-07T07:07:08.667Z');
    let midRangeDate = new Date('2019-10-17T07:07:08.667Z');

    expect(isInDateRange(testingDate, 5, midRangeDate, 5)).to.equal(false);
  });

  it("checks when behind higher date range", function() {
    let testingDate = new Date('2019-10-25T07:07:08.667Z');
    let midRangeDate = new Date('2019-10-17T07:07:08.667Z');

    expect(isInDateRange(testingDate, 5, midRangeDate, 5)).to.equal(false);
  });
});
