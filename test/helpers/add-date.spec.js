import { expect } from "chai";

import { addDate } from '../../dist';


describe("addDate()", function() {

  it("checks basic", function() {
    let sampleDate = new Date('2019-10-17T07:07:08.667Z');
    let date = addDate(sampleDate, 3);
    expect(date.getDate()).to.equal(20);
  });
});
