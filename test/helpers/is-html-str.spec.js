import { expect } from "chai";

import { isHtmlStr } from '../../dist';


describe("isHtmlStr()", function() {

  it("checks is HTML String", function() {
    expect(isHtmlStr('<h1>Test</h1>')).to.equal(true);
  });

  it("checks is NOT HTML String", function() {
    expect(isHtmlStr('Test')).to.equal(false);
  });
});
