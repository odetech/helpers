import { expect } from "chai";

import { arrayToObject } from '../../dist';


describe("arrayToObject()", function() {

  it("checks basic", function() {
    let result = arrayToObject(
      [
        {
          name: 'Vanielle'
        },
        {
          name: 'Osar'
        },
        {
          name: 'Thierry'
        }
      ],
      'name'
    );
    expect(JSON.stringify(result)).to.equal(JSON.stringify({
      Vanielle: [
        {
          name: 'Vanielle'
        }
      ],
      Osar: [
        {
          name: 'Osar'
        }
      ],
      Thierry: [
        {
          name: 'Thierry'
        }
      ]
    }));
  });
});
