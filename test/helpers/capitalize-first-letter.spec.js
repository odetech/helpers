import { expect } from "chai";

import { capitalizeFirstLetter } from '../../dist';


describe("capitalizeFirstLetter()", function() {

  it("checks basic", function() {
    expect(capitalizeFirstLetter("vanielle")).to.equal("Vanielle");
  });
});
