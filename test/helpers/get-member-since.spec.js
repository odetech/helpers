import { expect } from "chai";

import { getMemberSince } from '../../dist';


describe("getMemberSince()", function() {

  it("checks basic", function() {
    expect(getMemberSince("Tue Apr 02 2019 19:19:47 GMT+0800 (Hong Kong Standard Time)")).to.equal("2019/04/02");
  });
});
