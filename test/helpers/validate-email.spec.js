import { expect } from "chai";

import { validateEmail } from '../../dist';


describe("validateEmail()", function() {

  it("checks basic", function() {
    let email = validateEmail('vanielle@odecloud.com');
    expect(email).to.equal(true);
  });
});
