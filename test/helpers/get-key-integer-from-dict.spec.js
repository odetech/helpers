import { expect } from "chai";

import { getKeyIntegerFromDict } from '../../dist';


describe("getKeyIntegerFromDict()", function() {

  it("checks basic", function() {
    expect(getKeyIntegerFromDict({ '1': 'One', '2': 'Two', '3': 'Three' })).to.equal(3);
  });
});
