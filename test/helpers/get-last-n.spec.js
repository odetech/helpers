import { expect } from "chai";

import { getLastN } from '../../dist';


describe("getLastN()", function() {

  it("checks basic", function() {
    let lastObject = getLastN([1, 2, 3, 4, 5], 1);
    expect(`${lastObject}`).to.equal(`${[5]}`);
  });
});
