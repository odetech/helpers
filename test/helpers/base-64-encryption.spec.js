import { expect } from "chai";

import { Base64Encryption } from '../../dist';

let mockConsumerToken = '0aee0911a5036989a47eb93271eaa2fb7e569ccbe9131480b75916de462747d4';

describe("Base64Encryption()", function() {

  describe("generateConsumerToken()", function() {
    const B64 = Base64Encryption;

    it('checks basic', function() {
      expect(typeof B64.generateConsumerToken(mockConsumerToken)).to.equal("string");
    });
  });

  describe("base64Encode() and base64Decode()", function() {
    const b64 = Base64Encryption;

    const MOCK_TOKEN = 'TEST';

    const ENCODE = b64.base64Encode;
    const DECODE = b64.base64Decode;

    it('checks encode', function() {
      expect(typeof ENCODE(MOCK_TOKEN)).to.equal("string");
    });

    it('checks decode', function() {
      expect(typeof DECODE(MOCK_TOKEN)).to.equal("string");
    });

    it('checks encode then decode', function() {
      let encodedString = ENCODE(MOCK_TOKEN);
      let decodeString = DECODE(encodedString);
      expect(decodeString).to.equal(MOCK_TOKEN);
    });
  });
});
