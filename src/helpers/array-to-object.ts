export function arrayToObject(array: Array<object>, key: string): any {
  return array.reduce((obj: any, item: any) => {
    if (item[key] in obj) {
      obj[item[key]].push(item)
    } else {
      obj[item[key]] = [item]
    }

    return obj
  }, {});
}
