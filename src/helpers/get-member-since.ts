import MOMENT from 'moment';


export function getMemberSince(createdAt: Date, format: string = 'YYYY/MM/DD'): string {
  let date = MOMENT(createdAt).utc();

  return date.format(format);
}
