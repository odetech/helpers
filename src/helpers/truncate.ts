export function truncate(input: string, stopN: number) {
  return input.length > stopN ? `${input.substring(0, stopN)}...` : input;
}
