export function flattenDictionary(
  obj: any,
  callback?: (o: any, c: number) => void
): Array<{}> {

  let results = [];
  let count = 1

  for (let key in obj) {
    results.push(obj[key]);

    if (callback) {
      callback(obj[key], count);
    }

    count++;
  }

  return results;
}
