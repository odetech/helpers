export function getKeyIntegerFromDict(dict: {}): number {

  let highest: number = 0;

  for (let d in dict) {
    if (Number(d) > highest) {
      highest = Number(d);
    }
  }

  return highest;
}
