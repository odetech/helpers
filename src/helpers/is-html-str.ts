export function isHtmlStr(str: string): boolean {
  let reg: any = /<[a-z][\s\S]*>/i;
  return reg.test(str);
}
