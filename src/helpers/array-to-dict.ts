export function arrayToDict(array: Array<Object>, key: string, value: string) {

  return array.reduce((result: any, filter: any) => {
    result[filter[key]] = filter[value];
    return result;
  },{});
}
