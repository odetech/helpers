import { isValidDate } from './';


export function getDateInputValue(d: Date): string | undefined {
  if (!d) {
    return;
  }

  let local: Date = new Date(d);

  if (!isValidDate(local)) {
    return;
  }

  local.setMinutes(local.getMinutes() - local.getTimezoneOffset());

  if (!local.toJSON()) {
    return;
  }

  return local.toJSON().slice(0, 10);
}
