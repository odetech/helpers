// @ts-ignore

import Random from '@inway/meteor-random';
import { Buffer } from 'buffer';

export const Base64Encryption = {

  generateConsumerToken(key: any) {
    return this.base64Encode(`${key.toLowerCase()}-${Random.secret(10)}-${Random.hexString(10)}`);
  },

  base64Encode(unencoded: any) {
    return new Buffer(unencoded || '').toString('base64');
  },

  base64Decode(encoded: any) {
    return new Buffer(encoded || '', 'base64').toString('utf8');
  }
}
