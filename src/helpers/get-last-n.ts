export function getLastN(arr: Array<any>, n: number): Array<any> {
  return arr.slice(Math.max(arr.length - n, 0));
}
