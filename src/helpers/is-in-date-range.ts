import { addDate, deductDate } from './';


export function isInDateRange(
  testedDate: Date,
  minDate: number,
  midDate: Date,
  maxDate: number
): boolean {
  // Date being tested
  let controlledDate = new Date(testedDate);

  // Creating lower and upper range based upon the given `midDate`
  let currentDate = new Date(midDate);
  let lowerDateRange =  deductDate(currentDate, maxDate);
  let upperDateRange = addDate(currentDate, maxDate);

  // The `controlledDate` has to be HIGHER than the LOW RANGE
  if (lowerDateRange.getTime() < controlledDate.getTime()) {

    // The `controlledDate` has to be LOWER than the HIGH RANGE
    if (controlledDate.getTime() < upperDateRange.getTime()) {
      return true;
    }
  }

  return false;
}
